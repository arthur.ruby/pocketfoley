package fr.eni.pocketfoley.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Classe objet représentant une entité Box qui contient les boutons.
 * @param id L'identifiant unique de la box
 * @param title Le nom de la box
 */
@Entity(tableName = "table_box")
data class Box(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true)
    var id : Long = 0L,

    @ColumnInfo
    var title : String = ""
)