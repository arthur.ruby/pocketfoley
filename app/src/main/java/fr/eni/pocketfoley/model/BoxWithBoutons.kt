package fr.eni.pocketfoley.model

import androidx.room.Embedded
import androidx.room.Relation

/**
 * Classe associative qui représente l'association entre une box et des boutons
 * @param box une box qui contient plusieurs boutons
 * @param boutons les boutons qui appartiennent à une même box
 */
data class BoxWithBoutons(
    @Embedded
    val box: Box,

    @Relation(parentColumn = "id", entityColumn = "box")
    val boutons : List<Bouton>
)