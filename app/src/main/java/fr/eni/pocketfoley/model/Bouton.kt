package fr.eni.pocketfoley.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

/**
 * Classe objet représentant un bouton contenant un son à jouer.
 * @param id L'identifiant unique du bouton
 * @param title Le nom du bouton
 * @param image L'adresse du fichier image associé au bouton
 * @param externalSound L'adresse du fichier son externe associé au bouton
 * @param internalSound L'adresse du fichier du son interne associée au bouton
 * @param volume Le volume de l'extrait sonore associé au bouton
 * @param startTimeMillis Le début de l'extrait sonore associé au bouton, en millisecondes
 * @param stopTimeMillis La fin de l'extrait sonore associé au bouton, en millisecondes
 * @param box L'identifiant de la Box à laquelle est lié le bouton
 */
@Entity(tableName = "table_bouton",
        foreignKeys = [
            ForeignKey(
                entity = Box::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("box"))
        ]
)
data class Bouton(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true)
    var id : Long = 0L,

    @ColumnInfo
    var title : String = "",

    @ColumnInfo
    var image : String = "",

    @ColumnInfo(name = "external_sound")
    var externalSound : String = "",

    @ColumnInfo(name = "internal_sound")
    var internalSound : String = "",

    @ColumnInfo
    var volume : Int = 100,

    @ColumnInfo
    var loop : Boolean = false,

    @ColumnInfo(name = "start_time_millis")
    var startTimeMillis : Long = 0L,

    @ColumnInfo(name = "stop_time_millis")
    var stopTimeMillis : Long = 0L,

    @ColumnInfo(index = true)
    var box: Long = 0L

)