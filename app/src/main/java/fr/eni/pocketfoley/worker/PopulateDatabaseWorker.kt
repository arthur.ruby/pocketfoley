package fr.eni.pocketfoley.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.stream.JsonReader
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fr.eni.pocketfoley.dao.AppDatabase
import fr.eni.pocketfoley.dao.BoutonDao
import fr.eni.pocketfoley.dao.BoxDao
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.utils.BOUTON_DATA_FILENAME
import fr.eni.pocketfoley.utils.BOX_DATA_FILENAME
import timber.log.Timber
import java.lang.Exception

/**
 * Worker en charge de lancer la méthode de pré-remplissage de la BDD à la première ouverture de l'appli
 */
class PopulateDatabaseWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params)
{
    override suspend fun doWork(): Result
    {
        val database = AppDatabase.getAppDatabaseInstance(applicationContext)
        return try
        {
            Timber.i("Début du travail du Worker")
            populateDatabaseWithBoxes(database.boxDao)
            populateDatabaseWithBoutons(database.boutonDao)
            Timber.i("Fin du travail du Worker")
            Result.success()
        }
        catch (e : Exception)
        {
            Timber.i("Echec du travail du Worker")
            Result.retry()
        }
    }

    /**
     * Méthode en charge de remplir la table boutons de la BDD lors de la première ouverture
     * de l'appli avec les éléments du fichier JSON associé
     * @param boxDao la DAO des Boxes pour accéder à la méthode insert()
     **/
    private suspend fun populateDatabaseWithBoxes(boxDao: BoxDao)
    {
        Timber.i("Début de l'insertion des Boxes")
        try
        {
            applicationContext.assets.open(BOX_DATA_FILENAME).use {
                JsonReader(it.reader()).use {jsonReader ->
                    val boxeType = object : TypeToken<List<Box>>() {}.type
                    val boxes : List<Box> = Gson().fromJson(jsonReader, boxeType)
                    boxDao.insertAll(boxes)
                    Timber.i("Insertion des Boxes terminée")
                }
            }
        }
        catch (ex: Exception)
        {
            Timber.e("Erreur dans la population des Boxes en BDD: ${ex.message}")
            Result.failure()
        }
    }

    /**
     * Méthode en charge de remplir la table boutons de la BDD lors de la première ouverture
     * de l'appli avec les éléments du fichier JSON associé
     * @param boutonDao la DAO des boutons pour accéder à la méthode insert()
     **/
    private suspend fun populateDatabaseWithBoutons(boutonDao: BoutonDao)
    {
        Timber.i("Début de l'insertion des Boutons")
        try
        {
            applicationContext.assets.open(BOUTON_DATA_FILENAME).use {
                JsonReader(it.reader()).use {jsonReader ->
                    val boutonType = object : TypeToken<List<Bouton>>() {}.type
                    val boutons : List<Bouton> = Gson().fromJson(jsonReader, boutonType)
                    boutonDao.insertAll(boutons)
                    Timber.i("Insertion des Boutons terminée")
                }
            }
        }
        catch (ex: Exception)
        {
            Timber.e("Erreur dans la population des Boutons en BDD: ${ex.message}")
            Result.failure()
        }
    }
}