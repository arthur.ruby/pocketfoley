package fr.eni.pocketfoley.utils

import fr.eni.pocketfoley.R

enum class BoutonColor(val colorIdentifier: Int)
{
    COLOR1(R.color.color1),
    COLOR2(R.color.color2),
    COLOR3(R.color.color3),
    COLOR4(R.color.color4),
    COLOR5(R.color.color5),
    COLOR6(R.color.color6),
    COLOR7(R.color.color7),
    COLOR8(R.color.color8),
    COLOR9(R.color.color9),
    COLOR10(R.color.color10),
    COLOR11(R.color.color11),
    COLOR12(R.color.color12),
}