package fr.eni.pocketfoley.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.AssetFileDescriptor
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

/**
 * Extension function utilitaire en charge de formatter une [String] représentant une adresse pour
 * n'afficher que le nom du fichier à cette adresse.
 */
fun String.getTitleFromAddressString() = substringAfterLast('/').substringBefore('.').replace('_',' ').capitalize()

/**
 * Extension function utilitaire en charge de formatter une [String] représentant une adresse pour
 * n'afficher que le nom du fichier à cette adresse.
 */
fun String.getFileNameFromAddressString() = substringAfterLast('/')

/**
 * Extension function utilitaire en charge de convertir une durée de type Long en millisecondes
 * vers une durée de type Int en secondes.
 */
fun Long.convertMillisToSeconds() = TimeUnit.MILLISECONDS.toSeconds(this).toInt()

/**
 * Extension function utilitaire en charge de convertir une durée de type Int en secondes
 * vers une durée de type Long en millisecondes.
 */
fun Int.convertSecondstoMillis() = TimeUnit.SECONDS.toMillis(toLong())

/**
 * Extension function utilitaire en charge de convertir une durée de type Int en secondes
 * vers une durée de type Long en millisecondes puis de la formatter en String de type "mm:ss"
 */
@SuppressLint("SimpleDateFormat")
fun Int.convertSecondsToFormattedString() = SimpleDateFormat("mm:ss").format(TimeUnit.SECONDS.toMillis(toLong())).toString()

/**
 * Extension function utilitaire en charge de formatter un Int en String représentant un pourcentage
 */
fun Int.formatToPercentString() = String.format("%d%%", this)

/**
 * Méthode en charge de récupérer la durée d'un fichier audio d'après ses métadonnées
 * @param context le contexte pour récupérer l'applicationCOntext utilisée pour l'AssetFileDescriptor
 * @return la durée en millisecondes
 */
fun String.getInternalAudioFileDuration(context: Context) : Long
{
    val mmr = MediaMetadataRetriever()
    val descriptor : AssetFileDescriptor = context.applicationContext.resources.assets.openFd(this)
    mmr.setDataSource(descriptor.fileDescriptor, descriptor.startOffset, descriptor.length)
    descriptor.close()
    val strDuration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    return strDuration.toLong()
}

/**
 * Méthode en charge de récupérer le titre d'un fichier audio externe d'après ses métadonnées
 * @param context le contexte pour récupérer l'applicationCOntext utilisée pour le FileDescriptor
 * @return le titre de la piste audio
 */
fun String.getExternalAudioFileTitle(context: Context) : String
{
    val mmr = MediaMetadataRetriever()
    Timber.i("permissions: ${context.contentResolver.persistedUriPermissions.size}")
    val descriptor = context.contentResolver.openFileDescriptor(Uri.parse(this), "r")
    val fileDescriptor = descriptor!!.fileDescriptor
    mmr.setDataSource(fileDescriptor)
    descriptor.close()
    return mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
}

/**
 * Méthode en charge de récupérer la durée d'un fichier audio externe d'après ses métadonnées
 * @param context le contexte pour récupérer l'applicationCOntext utilisée pour le FileDescriptor
 * @return la durée en millisecondes
 */
fun String.getExternalAudioFileDuration(context: Context) : Long
{
    val mmr = MediaMetadataRetriever()
    Timber.i("permissions: ${context.contentResolver.persistedUriPermissions.size}")
    val descriptor = context.contentResolver.openFileDescriptor(Uri.parse(this), "r")
    val fileDescriptor = descriptor!!.fileDescriptor
    mmr.setDataSource(fileDescriptor)
    descriptor.close()
    val strDuration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    return strDuration.toLong()
}

/**
 * Méthode en charge de vérifier la validité du titre entré par l'utilisateur
 * @return true si le titre n'est pas vide ET fait max 12 caractères
 */
fun String.verifTitleLength() = isNotEmpty() && length <= MAX_CHARS_TITLE

/** Méthode en charge de masquer le clavier après un événement */
fun View.hideKeyboard()
{
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}