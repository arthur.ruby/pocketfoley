package fr.eni.pocketfoley.utils

/**
 * Constantes utilisées dans l'application
 */
const val DATABASE_NAME = "pocket_foley_database"
const val BOUTON_DATA_FILENAME = "json/boutons.json"
const val BOX_DATA_FILENAME = "json/boxes.json"
const val AUDIO_ASSETS_DIRECTORY = "audio/"
const val AUDIO_ASSETS_FILE_NAME = "audio"
const val BOX_ID_1 = 1L
const val BOX_ID_2 = 2L
const val BOX_ID_3 = 3L
const val OPEN_AUDIO_FILE_REQUEST_CODE = 4004
const val OPEN_IMAGE_FILE_REQUEST_CODE = 8008
const val MAX_CHARS_TITLE = 15
const val SPINNER_OPTION_TITLE = 1
const val SPINNER_OPTION_IMAGE = 2
const val SPINNER_OPTION_BOTH = 3
const val BOUTON_DISPLAY_PREFERENCES = "boutonDisplayPreferences"
const val NB_BOUTONS_PAR_BOX = 12