package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * Classe Factory pour créer un [DetailBoxViewModel]
 * @param boxId l'identifiant de la box associée au ViewModel
 * @param application le contexte d'application
 */
class DetailBoxViewModelFactory(private val boxId: Long, private val application: Application) : ViewModelProvider.Factory
{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(DetailBoxViewModel::class.java))
        {
            return DetailBoxViewModel(boxId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}