package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.repository.BoutonRepositorySqliteImpl
import fr.eni.pocketfoley.repository.RepositoryFactory
import fr.eni.pocketfoley.repository.SonRepositoryAudioAssetsImpl

class SonInterneViewModel(application: Application) : AndroidViewModel(application)
{
    /** Gestionnaire de coroutines qui permet de toutes les annuler quand viewModel n'est plus utilisé */
    private var viewModelJob = Job()

    /**
     * Scope de coroutines pour déterminer sur quel thread elles vont tourner.
     * Ici c'est pour mettre à jour l'IHM donc on reste sur le Main Thread.
     */
    private val sonInterneViewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /** L'instance du [SonRepositoryAudioAssetsImpl] qui n'a accès qu'aux méthodes de la DAO des sons internes */
    private val sonRepository : SonRepositoryAudioAssetsImpl = RepositoryFactory.getSonInterneRepository(application = application) as SonRepositoryAudioAssetsImpl

    /** La liste des sons internes remontée du [SonRepositoryAudioAssetsImpl] emballée dans un LiveData */
    val listeAudioAssets = sonRepository.audioAssets

    /** L'instance du [BoutonRepositorySqliteImpl] qui n'a accès qu'aux méthodes de la DAO des boutons */
    private val boutonRepository : BoutonRepositorySqliteImpl = RepositoryFactory.getBoutonRepository(application = application) as BoutonRepositorySqliteImpl

    /**
     * Propriété contenant le [Bouton] actuel visible de l'extérieur.
     * Elle récupère sa valeur de son équivalent dans le [BoutonRepositorySqliteImpl]
     * C'est elle qui va permettre la mise à jour automatique de l'IHM.
     */
    val bouton = boutonRepository.boutonData

    /**
     * Méthode en charge de modifier un [Bouton]
     * @param bouton le [Bouton] à modifier
     */
    fun editBouton(bouton: Bouton)
    {
        sonInterneViewModelScope.launch { boutonRepository.updateBouton(bouton) }
    }

    /** La propriété privée chargée de gérer la navigation après un clic sur un des [Bouton]s */
    private val _navigateToConfigBouton = MutableLiveData<Long>()

    /** La propriété publique chargée de gérer la navigation après un clic sur un des [Bouton]s */
    val navigateToConfigBouton
        get() = _navigateToConfigBouton

    /**
     * Méthode déclenchée par le clickListener sur un élément de la liste de [Bouton]s et qui permet
     * la navigation vers la page de configuration du bouton en assignant la valeur du [boutonId] à
     * la propriété [_navigateToConfigBouton].
     * @param boutonId l'identifiant du [Bouton] dont on affiche le détail
     * @return true pour avertir le onLongClickListener que le traitement a été effectué
     */
    fun onSonClicked(boutonId: Long): Boolean
    {
        _navigateToConfigBouton.value = boutonId
        return true
    }

    /** Méthode en charge de réinitialiser la propriété [_navigateToConfigBouton] une fois la navigation terminée */
    fun onConfigBoutonNavigated()
    {
        _navigateToConfigBouton.value = null
    }

}