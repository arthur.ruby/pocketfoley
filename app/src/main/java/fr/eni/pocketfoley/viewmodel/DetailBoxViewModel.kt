package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.repository.BoxRepositorySqliteImpl
import fr.eni.pocketfoley.repository.RepositoryFactory
import kotlinx.coroutines.*
import timber.log.Timber

/**
 * La classe de viewModel utilisée dans le fragment d'affichage des modifs d'une [Box]
 * @param boxId l'identifiant de la box associée au ViewModel
 * @param application le contexte d'application
 */
class DetailBoxViewModel(private val boxId: Long, application: Application) : ViewModel()
{
    /**
     * Gestionnaire de coroutines qui permet de toutes les annuler quand viewModel n'est plus utilisé
     */
    private var viewModelJob = Job()

    /**
     * Scope de coroutines pour déterminer sur quel thread elles vont tourner.
     * Ici c'est pour mettre à jour l'IHM donc on reste sur le Main Thread.
     */
    private val boxViewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * L'instance du Repository de Box qui n'a accès qu'aux méthodes de la DAO des boxes
     */
    private val repository : BoxRepositorySqliteImpl = RepositoryFactory.getBoxRepository(application) as BoxRepositorySqliteImpl

    /**
     * Propriété contenant la box actuelle visible de l'extérieur.
     * Elle récupère sa valeur de son équivalent dans le Repository
     * C'est elle qui va permettre la mise à jour automatique de l'IHM.
     */
    val box : LiveData<Box>
        get() = repository.boxData


    /**
     * Le bloc init{} est exécuté immédiatement à la création de ce boxViewModel.
     * Il se charge d'initialiser la valeur du LiveData contenant une Box.
     */
    init
    {
        Timber.i("initialisation de DetailBoxViewModel avec le boxId: $boxId")
        getBoxFromRepo(boxId)
    }

    /**
     * Méthode en charge de récupérer une box depuis le repo de façon indirecte:
     * Cela ne retourne pas une Box directement, mais ça déclenche la méthode
     * getBoxById du repository qui met à jour sa propriété Box en LiveData.
     * Cette propriété Box est ensuite observée par celle du ViewModel
     * @param id l'identifiant de la box à récupérer
     */
    private fun getBoxFromRepo(id: Long)
    {
        Timber.i("getBoxFromRepo(boxId = $id)")
        boxViewModelScope.launch {repository.getBoxById(id)}
    }

    /**
     * Méthode en charge de modifier une Box
     * @param box la box à modifier
     */
    fun editBox(box: Box)
    {
        boxViewModelScope.launch { repository.updateBox(box) }
    }

    /**
     * Appel de la méthode cancel() du job pour annuler toutes les coroutines
     * quand ce ViewModel est supprimé.
     */
    override fun onCleared()
    {
        super.onCleared()
        viewModelJob.cancel()
    }
}