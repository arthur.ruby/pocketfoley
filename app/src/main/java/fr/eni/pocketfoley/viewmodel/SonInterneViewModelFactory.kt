package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * Classe Factory pour créer un [SonInterneViewModel]
 * @param application le contexte d'application
 */
class SonInterneViewModelFactory(private val application: Application) : ViewModelProvider.Factory
{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(SonInterneViewModel::class.java))
        {
            return SonInterneViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}