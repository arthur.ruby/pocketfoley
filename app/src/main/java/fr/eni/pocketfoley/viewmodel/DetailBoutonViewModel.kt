package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.repository.BoutonRepositorySqliteImpl
import fr.eni.pocketfoley.repository.RepositoryFactory
import fr.eni.pocketfoley.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * La classe de viewModel utilisée dans le fragment d'affichage de la config d'un [Bouton]
 * @param boutonId l'identifiant du [Bouton] associé au ViewModel
 * @param application le contexte d'application
 */
class DetailBoutonViewModel(private val boutonId: Long, application: Application) : ViewModel()
{
    /** Gestionnaire de coroutines qui permet de toutes les annuler quand viewModel n'est plus utilisé */
    private var viewModelJob = Job()

    /**
     * Scope de coroutines pour déterminer sur quel thread elles vont tourner.
     * Ici c'est pour mettre à jour l'IHM donc on reste sur le Main Thread.
     */
    private val boutonViewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /** L'instance du [BoutonRepositorySqliteImpl] de [Bouton] */
    private val boutonRepository : BoutonRepositorySqliteImpl = RepositoryFactory.getBoutonRepository(application) as BoutonRepositorySqliteImpl

    /**
     * Propriété contenant le [Bouton] actuel visible de l'extérieur.
     * Elle récupère sa valeur de son équivalent dans le [BoutonRepositorySqliteImpl]
     * C'est elle qui va permettre la mise à jour automatique de l'IHM.
     */
    val bouton = boutonRepository.boutonData

    /**
     * Le bloc init{} est exécuté immédiatement à la création de ce viewModel.
     * Il se charge d'initialiser la valeur du LiveData contenant un bouton.
     */
    init
    {
        Timber.i("initialisation de DetailBoutonViewModel avec le boutonId: $boutonId")
        getBoutonFromRepoById(boutonId)
    }

    /**
     * Méthode en charge de récupérer un [Bouton] depuis le repo de façon indirecte:
     * Cette propriété [Bouton] est ensuite observée par celle du ViewModel
     * @param id l'identifiant du [Bouton] à récupérer
     */
    private fun getBoutonFromRepoById(id: Long)
    {
        Timber.i("getBoutonFromRepo(boutonId = $id)")
        boutonViewModelScope.launch {boutonRepository.getBoutonById(id)}
    }

    /** Propriété contenant le numéro du son à jouer */
    val son = Transformations.map(bouton){
        Timber.i("Transformation LiveData titre Son avec bouton ${it.title}")
        if(it.externalSound.isNotEmpty())
        {
            it.externalSound.getExternalAudioFileTitle(application)
        }
        else
        {
            it.internalSound
        }}

    //TODO régler le problème des Transformations.map qui s'exécutent avant que le bouton soit mis
    // à jour. Ce qui provoque un décalage dans l'affichage: on clique sur le bouton 1: startTime
    // et stopTime affichent 0. On clique sur le bouton 2: starTime et StopTime affichent celui du 1
    /**
     * Propriété contenant l'attribut startTimeMillis du [Bouton] converti en [Integer]
     * pour l'afficher sur la SeekBar de l'IHM
     */
    val startTimeToInt = Transformations.map(bouton){
        Timber.i("Transformation LiveData startTime avec bouton ${it.title}")
        it.startTimeMillis.convertMillisToSeconds() }

    /**
     * Propriété contenant l'attribut stopTimeMillis du [Bouton] converti en [Integer]
     * pour l'afficher sur la SeekBar de l'IHM
     */
    val stopTimeToInt = Transformations.map(bouton){
        Timber.i("Transformation LiveData stopTime avec bouton ${it.title}")
        it.stopTimeMillis.convertMillisToSeconds()
        }

    /** Propriété contenant la durée maximale du fichier audio associé pour le max de la seekbar */
    val maxDuration = Transformations.map(bouton) {
        Timber.i("Transformation LiveData maxDuration avec bouton ${it.title}")
        if (it.externalSound.isNotEmpty())
        {
            it.externalSound.getExternalAudioFileDuration(application).convertMillisToSeconds()
        }
        else
        {
            (AUDIO_ASSETS_DIRECTORY + it.internalSound).getInternalAudioFileDuration(application).convertMillisToSeconds()
        }
    }

    /** Propriété contenant la durée maximale du fichier audio associé moins une seconde pour le max de la seekbar de startTime */
    val maxStartTime = Transformations.map(maxDuration) { it.dec() }

    /**
     * Méthode en charge de modifier un [Bouton].
     * @param bouton le [Bouton] à modifier
     */
    fun editBouton(bouton: Bouton)
    {
        boutonViewModelScope.launch { boutonRepository.updateBouton(bouton) }
    }

    /** Appel de la méthode cancel() du job pour annuler toutes les coroutines quand ce ViewModel est supprimé. */
    override fun onCleared()
    {
        Timber.i("onCleared() du ViewModel")
        super.onCleared()
        viewModelJob.cancel()
    }
}