package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.model.BoxWithBoutons
import fr.eni.pocketfoley.repository.BoutonRepositorySqliteImpl
import fr.eni.pocketfoley.repository.BoxRepositorySqliteImpl
import fr.eni.pocketfoley.repository.RepositoryFactory

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * La classe de viewModel utilisée dans le fragment d'affichage des [Bouton]
 * @param boxId l'identifiant de la [Box] associée au ViewModel
 * @param application le contexte d'application
 */
class BoxWithBoutonsViewModel(private val boxId: Long, application: Application) : ViewModel()
{
    /** Gestionnaire de coroutines qui permet de toutes les annuler quand viewModel n'est plus utilisé */
    private var viewModelJob = Job()

    /**
     * Scope de coroutines pour déterminer sur quel thread elles vont tourner.
     * Ici c'est pour mettre à jour l'IHM donc on reste sur le Main Thread.
     */
    private val boxWithBoutonsViewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /** L'instance du Repository de [Box] qui n'a accès qu'aux méthodes de la DAO des boxes */
    private val boxRepository = RepositoryFactory.getBoxRepository(application) as BoxRepositorySqliteImpl

    /** L'instance du Repository de [Bouton] qui n'a accès qu'aux méthodes de la DAO des boutons */
    private val boutonRepository = RepositoryFactory.getBoutonRepository(application) as BoutonRepositorySqliteImpl

    /**
     * Propriété contenant une entité relationnelle [BoxWithBoutons] remontée du [BoxRepositorySqliteImpl]
     * en lui transmettant un l'identifiant de la [Box] à récupérer.
     */
    val boxWithBoutons = boxRepository.boxWithBoutonsData

    private fun getBoxWithBoutons(boxId: Long)
    {
        boxWithBoutonsViewModelScope.launch { boxRepository.getBoxWithBoutonsById(boxId) }
    }

    /** Propriété contenant la liste de toutes les [Box]es remontées du [BoxRepositorySqliteImpl] */
    val boxes = boxRepository.boxesData

    init
    {
        Timber.i("Initialisation de BoxWithBoutonViewModel")
        getBoxWithBoutons(boxId = boxId)
    }
    /**
     * La propriété privée chargée de gérer la navigation après un clic sur un des [Bouton]s
     */
    private val _navigateToConfigBouton = MutableLiveData<Long>()

    /**
     * La propriété publique chargée de gérer la navigation après un clic sur un des [Bouton]s
     */
    val navigateToConfigBouton : LiveData<Long>
        get() = _navigateToConfigBouton

    /**
     * Méthode déclenchée par le clickListener sur un élément de la liste de [Bouton]s et qui permet
     * la navigation vers la page de configuration du bouton en assignant la valeur du [boutonId] à
     * la propriété [_navigateToConfigBouton].
     * @param boutonId l'identifiant du [Bouton] dont on affiche le détail
     * @return true pour avertir le onLongClickListener que le traitement a été effectué
     */
    fun onBoutonLongClicked(boutonId: Long): Boolean
    {
        _navigateToConfigBouton.value = boutonId
        return true
    }

    /** Méthode en charge de réinitialiser la propriété [_navigateToConfigBouton] une fois la navigation terminée */
    fun onConfigBoutonNavigated()
    {
        _navigateToConfigBouton.value = null
    }

    /**
     * Méthode en charge de mettre à jour le numéro du bouton qui a été cliqué
     * dans la propriété [boutonClicked]
     * @param boutonId l'identifiant du bouton sur lequel on a cliqué
     */
    fun onBoutonClicked(boutonId: Long)
    {
        getBoutonFromRepo(boutonId)
    }

    val boutonClicked = boutonRepository.boutonData
    /**
     * Méthode en charge de récupérer un [Bouton] depuis le repo de façon indirecte:
     * Cette propriété [Bouton] est ensuite observée par celle du ViewModel
     * @param boutonId l'identifiant du [Bouton] à récupérer
     */
    private fun getBoutonFromRepo(boutonId: Long)
    {
        boxWithBoutonsViewModelScope.launch { boutonRepository.getBoutonById(boutonId) }
    }

    override fun onCleared()
    {
        super.onCleared()
        viewModelJob.cancel()
    }
}