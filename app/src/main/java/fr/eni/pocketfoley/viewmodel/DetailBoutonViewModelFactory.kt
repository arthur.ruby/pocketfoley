package fr.eni.pocketfoley.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


/**
 * Classe Factory pour créer un [DetailBoutonViewModel]
 * @param boutonId l'identifiant du bouton associée au ViewModel
 * @param application le contexte d'application
 */
class DetailBoutonViewModelFactory(private val boutonId: Long, private val application: Application) : ViewModelProvider.Factory
{
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(DetailBoutonViewModel::class.java))
        {
            return DetailBoutonViewModel(boutonId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}