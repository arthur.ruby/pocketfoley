package fr.eni.pocketfoley.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.utils.DATABASE_NAME
import fr.eni.pocketfoley.worker.PopulateDatabaseWorker
import timber.log.Timber

/**
 * Classe abstraite représentant la base de données de l'application et qui hérite de [RoomDatabase]
 */
@Database(entities = [Box::class, Bouton::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase()
{
    /**
     * La DAO de l'entité [Bouton] permettant d'utiliser ses méthodes d'accès à la base de données
     */
    abstract val boutonDao : BoutonDao

    /**
     * La DAO de l'entité [Box] permettant d'utiliser ses méthodes d'accès à la base de données
     */
    abstract val boxDao : BoxDao

    companion object
    {
        @Volatile
        private var INSTANCE : AppDatabase? = null

        /**
         * Méthode en charge de retourner le Singleton de la BDD
         * @param context le contexte de l'application nécessaire pour le constructeur de BDD
         * @return l'instance de la BDD nouvellement créée ou déjà existante
         */
        fun getAppDatabaseInstance(context: Context) : AppDatabase
        {
            Timber.i("return getAppDatabaseInstance()")
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }

        /**
         * Méthode en charge de construire la BDD lors de la première ouverture de l'application
         * en faisant appel à un Worker
         */
        private fun buildDatabase(context: Context): AppDatabase
        {
            Timber.i("Création de la BDD")
            return Room.databaseBuilder(context.applicationContext,
                AppDatabase::class.java, DATABASE_NAME)
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        val request = OneTimeWorkRequestBuilder<PopulateDatabaseWorker>().build()
                        WorkManager.getInstance(context).enqueue(request)
                    }
                })
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}