package fr.eni.pocketfoley.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.model.BoxWithBoutons

/**
 * Interface représentant la DAO de l'entité Box.
 * Contient les méthodes permettant l'accès à la base de données pour cette entité.
 */
@Dao
interface BoxDao
{
    /**
     * Méthode de la DAO en charge d'insérer toutes les boxes dans la BDD
     * @param boxes la liste des boxes à insérer
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(boxes: List<Box>)

    /**
     * Méthode de la DAO en charge de mettre à jour une box dans la BDD
     * @param box la box à mettre à jour
     */
    @Update
    suspend fun update(box : Box)

    /**
     * Méthode de la DAO en charge de récupérer une box depuis la BDD
     * @param id l'identifiant de la box à récupérer
     * @return la box à récupérer
     */
    @Query("SELECT * FROM table_box WHERE id = :id")
    fun get(id : Long) : Box

    /**
     * Méthode de la DAO en charge de récupérer toutes les boxes dans la BDD
     * @return la liste des boxes en BDD emballée dans un LiveData
     */
    @Query("SELECT * FROM table_box ORDER BY id ASC")
    fun getAll() : LiveData<List<Box>>

    /**
     * Méthode de la DAO en charge de récupérer les infos des tables [Box] et [Bouton] pour
     * que Room fasse la relation entre les deux entités.
     * @param id L'identifiant de la [Box] dont on veut récupérer les boutons associés
     * @return L'association [BoxWithBoutons] à récupérer
     */
    @Transaction
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * FROM table_bouton AS btn JOIN table_box AS bx ON bx.id = btn.box WHERE box = :id")
    fun getOneBoxWithBoutonsById(id: Long) : BoxWithBoutons
}