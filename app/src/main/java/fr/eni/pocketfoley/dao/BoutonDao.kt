package fr.eni.pocketfoley.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.eni.pocketfoley.model.Bouton


/**
 * Interface représentant la DAO de l'entité Bouton.
 * Contient les méthodes permettant l'accès à la base de données pour cette entité.
 */
@Dao
interface BoutonDao
{
    /**
     * Méthode de la DAO en charge d'insérer tous les Bouton dans la BDD
     * @param boutons la liste des Bouton à insérer
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(boutons: List<Bouton>)

    /**
     * Méthode de la DAO en charge de mettre à jour un bouton en BDD
     * @param bouton Le bouton à mettre à jour
     */
    @Update
    fun update(bouton: Bouton)

    /**
     * Méthode de la DAO en charge de récupérer un bouton de la BDD
     * @param id l'identifiant du bouton à récupérer
     * @return Le bouton à récupérer
     */
    @Query("SELECT * FROM table_bouton WHERE id = :id")
    fun get(id: Long) : Bouton

    /**
     * Méthode de la DAO en charge de récupérer les boutons appartenant à une Box précise de la BDD
     * @param id L'identifiant de la Box dont on veut récupérer les boutons associés
     * @return La liste des boutons à récupérer emballée dans un LiveData
     */
    @Query("SELECT * FROM table_bouton WHERE box = :id")
    fun getAllByBoxId(id: Long) : LiveData<List<Bouton>>
}