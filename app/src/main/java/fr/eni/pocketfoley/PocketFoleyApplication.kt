package fr.eni.pocketfoley

import android.app.Application
import com.facebook.stetho.Stetho
import timber.log.Timber

/**
 * Classe permettant de redéfinir la classe d'Application
 * pour utiliser le gestionnaire de Logs "Timber"
 */
class PocketFoleyApplication : Application()
{
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Stetho.initializeWithDefaults(this)
    }
}