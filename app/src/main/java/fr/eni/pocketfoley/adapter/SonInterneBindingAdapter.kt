package fr.eni.pocketfoley.adapter

import android.widget.TextView
import androidx.databinding.BindingAdapter
import fr.eni.pocketfoley.utils.getTitleFromAddressString

@BindingAdapter("sonTitre")
fun TextView.setSonTitle(item: String?) = item?.let { text = it.getTitleFromAddressString() }