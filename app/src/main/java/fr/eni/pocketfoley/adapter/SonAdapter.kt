package fr.eni.pocketfoley.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.eni.pocketfoley.databinding.RecyclerviewListeSonsItemBinding

class SonAdapter(val clickListener: SonListener) : ListAdapter<String, SonAdapter.SonViewHolder>(SonDiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SonViewHolder.from(parent)

    override fun onBindViewHolder(holder: SonViewHolder, position: Int) = holder.bind(getItem(position)!!, clickListener)


    /**
     * Classe interne représentant le type de viewHolder qu'on va créer.
     * Le constructeur est privé car n'est appelé que dans la classe, depuis le companion object.
     * @param binding la vue associée au viewHolder
     */
    class SonViewHolder private constructor(val binding: RecyclerviewListeSonsItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        /**
         * Méthode en charge de faire le lien entre le view Item et les données à afficher
         * @param currentItem la [String] à relier à la vue
         * @param clickListener le clickListener correspondant à l'élément passé en param au constructeur par le fragment
         */
        fun bind(currentItem: String, clickListener: SonListener)
        {
            binding.son = currentItem
            binding.executePendingBindings()
            binding.layoutListeSonItem.setOnClickListener{
                clickListener.onClick(currentItem)
            }
        }

        companion object
        {
            /**
             * Méthode en charge de créer un [SonViewHolder] depuis un contexte particulier
             * @param parent le [ViewGroup] dans lequel s'insère le viewHolder (ici un [RecyclerView])
             * @return le [SonViewHolder] spécifique à un son
             */
            fun from(parent: ViewGroup): SonViewHolder
            {
                // On crée un inflater avec le context du viewGroup(RecyclerView) dans lequel s'insère le view item
                val inflater: LayoutInflater = LayoutInflater.from(parent.context)
                // On établit le lien avec le DataBinding du layout
                val binding = RecyclerviewListeSonsItemBinding.inflate(inflater, parent,false)

                // On transforme ensuite la View en un ViewHolder et on le retourne
                return SonViewHolder(binding)
            }
        }
    }
}

/**
 * Classe en charge de gérer les callbacks de DiffUtil qui permet de vérifier si les éléments de la
 * liste ont changé de place dans la liste ou si leur contenu a changé afin de mettre à jour les
 * éléments affichés dans le RecyclerView
 */
class SonDiffCallback : DiffUtil.ItemCallback<String>()
{
    override fun areItemsTheSame(oldItem: String, newItem: String) = oldItem == newItem

    override fun areContentsTheSame(oldItem: String, newItem: String) = oldItem == newItem
}

/**
 * Classe en charge de gérer le clickListener sur un élément du RecyclerView
 */
class SonListener(val clickListener : (sonAddress : String) -> Unit)
{
    fun onClick(sonAddress : String) = clickListener(sonAddress)
}