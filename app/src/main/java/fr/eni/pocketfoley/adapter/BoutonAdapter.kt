package fr.eni.pocketfoley.adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.eni.pocketfoley.databinding.RecyclerviewListeBoutonsItemBinding
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.utils.*

/**
 * Un Adapter de type [ListAdapter] qui fournit une liste de [Bouton]s à un RecyclerView
 * @param clickListener le clickListener correspondant à l'élément passé en param au constructeur par le fragment
 * @param longClickListener le longClickListener correspondant à l'élément passé en param au constructeur par le fragment
 */
class BoutonAdapter(val clickListener : BoutonListener, private val longClickListener : BoutonLongListener, val context: Context) : ListAdapter<Bouton, BoutonAdapter.BoutonViewHolder>(BoutonDiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BoutonViewHolder.from(parent)

    override fun onBindViewHolder(holder: BoutonViewHolder, position: Int)
    {
        holder.bind(getItem(position)!!, clickListener, longClickListener, context)

    }

    /**
     * Classe interne représentant le type de viewHolder qu'on va créer.
     * Le constructeur est privé car n'est appelé que dans la classe, depuis le companion object.
     * @param binding la vue associée au viewHolder
     */
    class BoutonViewHolder(val binding: RecyclerviewListeBoutonsItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        /**
         * Méthode en charge de faire le lien entre le view Item et les données à afficher
         * @param currentItem le [Bouton] à relier à la vue
         * @param clickListener le clickListener correspondant à l'élément passé en param au constructeur par le fragment
         */
        fun bind(currentItem: Bouton, clickListener: BoutonListener, longClickListener : BoutonLongListener, context: Context)
        {
            binding.bouton = currentItem
            binding.executePendingBindings()

            // On vérifie l'option d'affichage choisie (titre, image ou les deux) et on affiche ou
            // masque les champs en conséquence.
            val preferences: SharedPreferences = context.applicationContext.getSharedPreferences(BOUTON_DISPLAY_PREFERENCES, Context.MODE_PRIVATE)
            val option = preferences.getInt(currentItem.id.toString(), SPINNER_OPTION_TITLE)
            binding.ivBoutonRecyclerItemImage.visibility =
                if (option == SPINNER_OPTION_IMAGE || option == SPINNER_OPTION_BOTH)
                {
                    View.VISIBLE
                }
                else
                {
                    View.INVISIBLE
                }
            binding.tvBoutonRecyclerItemTitle.visibility =
                if (option == SPINNER_OPTION_TITLE || option == SPINNER_OPTION_BOTH)
                {
                    View.VISIBLE
                }
                else
                {
                    View.INVISIBLE
                }

            // on met en place un longClickListener sur le layout qui contient un élément entier
            // du RecyclerView car c'est impossible de déclarer le onLongClick direct dans le.xml
            binding.layoutListeBoutonItem.setOnLongClickListener{
                // Lorsqu'un longClick est détecté, la méthode onLongClick du BoutonLongListener est déclenchée
                longClickListener.onLongClick(currentItem)
            }
            // on relie la data clicklistener du .xml au clicklistener passé en param
            // le onClick est défini directement dans le .xml
            // Lorsqu'un click est détecté, la méthode onClick du BoutonListener est déclenchée
            binding.clickListener = clickListener
            val color = context.getColor(BoutonColor.values()[adapterPosition].colorIdentifier)
            binding.layoutListeBoutonItem.setBackgroundColor(color)
            binding.tvBoutonRecyclerItemTitle.setBackgroundColor(color)
            binding.ivBoutonRecyclerItemLoopIcon.setBackgroundColor(color)
            binding.ivBoutonRecyclerItemPlayingIcon.setBackgroundColor(color)
        }

        companion object
        {
            /**
             * Méthode en charge de créer un [BoutonViewHolder] depuis un contexte particulier
             * @param parent le [ViewGroup] dans lequel s'insère le viewHolder (ici un [RecyclerView])
             * @return le [BoutonViewHolder] spécifique à un [Bouton]
             */
            fun from(parent: ViewGroup): BoutonViewHolder
            {
                // On crée un inflater avec le context du viewGroup(RecyclerView) dans lequel s'insère le view item
                val inflater: LayoutInflater = LayoutInflater.from(parent.context)

                // On établit le lien avec le DataBinding du layout
                val binding = RecyclerviewListeBoutonsItemBinding.inflate(inflater, parent, false)

                // On transforme ensuite la View en un ViewHolder et on le retourne
                return BoutonViewHolder(binding)
            }
        }
    }
}
/**
 * Classe en charge de gérer les callbacks de DiffUtil qui permet de vérifier si les éléments de la
 * liste ont changé de place dans la liste ou si leur contenu a changé afin de mettre à jour les
 * éléments affichés dans le RecyclerView
 */
class BoutonDiffCallback : DiffUtil.ItemCallback<Bouton>()
{
    override fun areItemsTheSame(oldItem: Bouton, newItem: Bouton) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Bouton, newItem: Bouton) = oldItem == newItem
}

/**
 * Classe en charge de gérer le clickListener sur un élément du RecyclerView
 * Passée en paramètre au constructeur de l'adapter
 */
class BoutonListener(val clickListener : (boutonId : Long) -> Unit)
{
    /**
     * Lorsqu'elle est appelée, cette méthode déclenche la méthode du clickListener
     * passée en paramètre (lors de l'appel au constructeur de la classe, à la création
     * de l'adatper)
     */
    fun onClick(bouton: Bouton) = clickListener(bouton.id)
}
/**
 * Classe en charge de gérer le longClickListener sur un élément du RecyclerView
 * Passée en paramètre au constructeur de l'adapter
 */
class BoutonLongListener(val clickListener : (boutonId : Long) -> Boolean)
{
    /**
     * Lorsqu'elle est appelée, cette méthode déclenche la méthode du clickListener
     * passée en paramètre (lors de l'appel au constructeur de la classe, à la création
     * de l'adatper)
     */
    fun onLongClick(bouton: Bouton) : Boolean
    {
       return clickListener(bouton.id)
    }
}