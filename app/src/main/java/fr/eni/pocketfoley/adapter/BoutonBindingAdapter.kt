package fr.eni.pocketfoley.adapter

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.work.impl.model.Preference
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.utils.*
import timber.log.Timber

@BindingAdapter("boutonTitre")
fun TextView.setBoutonTitle(item: Bouton?)= item?.let { text = it.title }

@BindingAdapter("boutonId")
fun TextView.setBoutonId(item: Bouton?)= item?.let { text = it.id.toString() }

@BindingAdapter("boutonSound")
fun TextView.setInternalSound(item: Bouton?)= item?.let {
    text = if (it.externalSound.isNotEmpty())
    {
        it.externalSound
    }
    else
    {
        it.internalSound
    }
}

@BindingAdapter("boutonImageUri")
fun ImageView.setBoutonImageUri(item: Bouton?) = item?. let {
    if (it.image.isNotEmpty())
    {
        try
        {
            setImageURI(Uri.parse(it.image))
        }
        catch (e : Exception)
        {
            Timber.e("Erreur dans setImage: ${e.message}")
            context.revokeUriPermission(Uri.parse(it.image), Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
    }
}

@BindingAdapter("boutonBoucleIcon")
fun ImageView.setBoutonLoopIcon(item: Bouton?)= item?.let {
    visibility = if (it.loop)
    {
        View.VISIBLE
    }
    else
    {
        View.INVISIBLE
    }
}