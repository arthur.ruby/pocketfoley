package fr.eni.pocketfoley.repository

import android.app.Application

class RepositoryFactory
{
    companion object
    {
        fun getBoxRepository(application: Application) : BoxRepository
        {
            return BoxRepositorySqliteImpl.getBoxRepoInstance(application)
        }

        fun getBoutonRepository(application: Application) : BoutonRepository
        {
            return BoutonRepositorySqliteImpl.getBoutonRepoInstance(application)
        }

        fun getSonInterneRepository(application: Application) : SonRepository
        {
            return SonRepositoryAudioAssetsImpl.getSonRepoInstance(application)
        }
    }
}