package fr.eni.pocketfoley.repository

import android.app.Application

/**
 * Interface chargée de faire l'abstraction de ce que contient le Repository
 */
interface SonRepository
{
    fun getSonsInternes()
}