package fr.eni.pocketfoley.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import fr.eni.pocketfoley.dao.AppDatabase
import fr.eni.pocketfoley.dao.BoxDao
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.model.BoxWithBoutons
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

/**
 * Classe Repository chargée de faire abstraction des différentes sources de données des boxes.
 * Ici on n'a qu'une BDD, mais si on avait d'autres sources (internet par ex), alors les classes
 * qui ont besoin de données n'auraient qu'à appeler le Repository sans se soucier d'où se trouvent
 * lesdites données.
 * Possède un constructeur privé pour être instanciée comme un Singleton dans le companion object
 */
class BoxRepositorySqliteImpl constructor(application: Application) : BoxRepository
{

    /**
     * La DAO et par extension l'instance de la BDD dont le Repository a besoin
     */
    private val boxDao : BoxDao = AppDatabase.getAppDatabaseInstance(application).boxDao

    /**
     * La liste de toutes les Boxes disponibles en BDD emballée dans un LiveData
     */
    val boxesData : LiveData<List<Box>> = getAllBoxes()

    /**
     * La liste de toutes les associations BoxWithBoutons disponibles en BDD emballée dans un LiveData
     */
    private val _boxWithBoutonsData = MutableLiveData<BoxWithBoutons>()
    val boxWithBoutonsData : LiveData<BoxWithBoutons>
            get() = _boxWithBoutonsData

    /**
     * Propriété contenant la box actuelle.
     * De type MutableLiveData, ce qui la rend observable par le viewModel et permet la màj automatique de l'IHM,
     * mais aussi et surtout modifiable. L'observer est sur cet objet-ci, et c'est son contenu qui change.
     * On ne crée donc pas un nouvel observer sur chaque nouvel objet Box remonté de la BDD.
     * Elle est privée pour être ne pas être modifiable de l'extérieur.
     */
    private val _boxData = MutableLiveData<Box>()

    /**
     * Propriété contenant la box actuelle visible de l'extérieur.
     * Elle récupère sa valeur de son équivalent privée "_boxData".
     */
    val boxData : LiveData<Box>
        get() = _boxData

    /**
     * Méthode en charge de récupérer la liste de toutes les [Box]es de la BDD
     */
    override fun getAllBoxes(): LiveData<List<Box>> = boxDao.getAll()

    /**
     * Méthode du Repository en charge de mettre à jour une box.
     * De type suspend car doit absolument être appelée depuis une coroutine.
     * @param box la box à mettre à jour
     */
    override suspend fun updateBox(box: Box)
    {
        withContext(Dispatchers.IO){ boxDao.update(box) }
        Timber.i("updateBox() || ancienneBox =${_boxData.value} || nouvelleBox = $box)")
        _boxData.value = box
    }

    /**
     * Méthode du Repository en charge de récupérer une box par son identifiant.
     * De type suspend car doit absolument être appelée depuis une coroutine.
     * @param id L'identifiant de la box à récupérer
     */
    override suspend fun getBoxById(id: Long)
    {
        _boxData.value =  withContext(Dispatchers.IO){boxDao.get(id)}
        Timber.i("getBoxById(id= $id) = ${_boxData.value})")
    }

    /**
     * Méthode en charge de récupérer l'association [BoxWithBoutons] de la BDD, soit la liste des
     * [Bouton]s associés à une [Box] précise grâce à la méthode équivalente de la [BoxDao].
     * @param id l'identifiant de la [Box] dont on veut récupérer les infos et la liste de [Bouton]s
     * @return L'association [BoxWithBoutons] emballée dans un [LiveData]
     */
    override suspend fun getBoxWithBoutonsById(id: Long)
    {
        _boxWithBoutonsData.value = withContext(Dispatchers.IO){boxDao.getOneBoxWithBoutonsById(id)}
    }

    companion object
    {
        @Volatile
        private var instance : BoxRepositorySqliteImpl? = null

        /**
         * Méthode en charge de créer ou récupérer l'instance unique de Repository
         * Permet d'en faire un Singleton
         * @return l'instance du repository
         */
        fun getBoxRepoInstance(application: Application) : BoxRepositorySqliteImpl
        {
            return instance?: synchronized(this) {
                instance?: BoxRepositorySqliteImpl(application)
                        .also { instance = it }
            }
        }
    }
}