package fr.eni.pocketfoley.repository

import fr.eni.pocketfoley.model.Bouton

/**
 * Interface chargée de faire l'abstraction de ce que contient le Repository
 */
interface BoutonRepository
{
    suspend fun updateBouton(bouton: Bouton)

    suspend fun getBoutonById(id: Long)
}