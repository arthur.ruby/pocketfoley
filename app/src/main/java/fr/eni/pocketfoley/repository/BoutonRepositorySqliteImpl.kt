package fr.eni.pocketfoley.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import fr.eni.pocketfoley.dao.AppDatabase
import fr.eni.pocketfoley.dao.BoutonDao
import fr.eni.pocketfoley.model.Bouton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Classe Repository chargée de faire abstraction des différentes sources de données des boxes.
 * Ici on n'a qu'une BDD, mais si on avait d'autres sources (internet par ex), alors les classes
 * qui ont besoin de données n'auraient qu'à appeler le Repository sans se soucier d'où se trouvent
 * lesdites données.
 * Possède un constructeur privé pour être instanciée comme un Singleton dans le companion object
 */
class BoutonRepositorySqliteImpl private constructor(application: Application) : BoutonRepository
{
    /** La DAO et par extension l'instance de la BDD dont le Repository a besoin */
    private val boutonDao : BoutonDao = AppDatabase.getAppDatabaseInstance(application).boutonDao

    /** L'objet [MutableLiveData] privé contenant un [Bouton] remonté de la BDD */
    private val _boutonData = MutableLiveData<Bouton>()

    /**
     * L'objet [LiveData] public contenant un [Bouton] remonté de la BDD
     * Tire sa valeur de son parent privé [_boutonData]
     */
    val boutonData : LiveData<Bouton>
        get() = _boutonData

    /**
     * Méthode du [BoutonRepositorySqliteImpl] en charge de mettre à jour un [Bouton]
     * @param bouton Le [Bouton] à mettre à jour
     */
    override suspend fun updateBouton(bouton: Bouton)
    {
        withContext(Dispatchers.IO){boutonDao.update(bouton)}
//        Timber.i("updateBouton() =${_boutonData.value}")
        _boutonData.value = bouton
    }

    /**
     * Méthode du [BoutonRepositorySqliteImpl] en charge de récupérer un [Bouton]
     * et de le stocker dans la propriété [_boutonData]
     * @param id l'identifiant du [Bouton] à récupérer
     */
    override suspend fun getBoutonById(id: Long)
    {
        _boutonData.value =  withContext(Dispatchers.IO){boutonDao.get(id)}
//        Timber.i("getBoutonById(id= $id) = ${_boutonData.value})")
    }

    companion object
    {
        @Volatile
        private var instance : BoutonRepositorySqliteImpl? = null

        /**
         * Méthode en charge de créer ou récupérer l'instance unique de [BoutonRepositorySqliteImpl]
         * Permet d'en faire un Singleton
         * @return l'instance du [BoutonRepositorySqliteImpl]
         */
        fun getBoutonRepoInstance(application: Application) =
            instance?: synchronized(this) {
                instance?: BoutonRepositorySqliteImpl(application)
                        .also { instance = it }
            }
    }
}