package fr.eni.pocketfoley.repository

import androidx.lifecycle.LiveData
import fr.eni.pocketfoley.model.Box

/**
 * Interface chargée de faire l'abstraction de ce que contient le Repository
 */
interface BoxRepository
{
    fun getAllBoxes(): LiveData<List<Box>>

    suspend fun getBoxById(id: Long)

    suspend fun getBoxWithBoutonsById(id: Long)

    suspend fun updateBox(box: Box)
}