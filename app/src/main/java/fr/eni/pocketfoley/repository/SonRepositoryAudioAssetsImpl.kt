package fr.eni.pocketfoley.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import fr.eni.pocketfoley.utils.AUDIO_ASSETS_DIRECTORY
import fr.eni.pocketfoley.utils.AUDIO_ASSETS_FILE_NAME
import timber.log.Timber
import java.lang.StringBuilder

/**
 * Classe Repository chargée de faire abstraction des différentes sources de données des sons internes.
 * Possède un constructeur privé pour être instanciée comme un Singleton dans le companion object
 * Permet de récupérer la liste des sons présents dans le dossier Assets/audio
 */
class SonRepositoryAudioAssetsImpl private constructor(val application: Application) : SonRepository
{
    /** Propriété représentant la liste des sons emballée dans un LiveData */
    val audioAssets : LiveData<List<String>>
        get() = _audioAssets
    private var _audioAssets = MutableLiveData<List<String>>()

    /** Méthode en charge de récupérer la liste des sons présents dans le dossier assets/audio */
    override fun getSonsInternes()
    {
       _audioAssets.value = application.assets.list(AUDIO_ASSETS_FILE_NAME)!!.toList()
    }

    init
    {
        getSonsInternes()
    }

    companion object
    {
        @Volatile
        private var instance : SonRepositoryAudioAssetsImpl? = null

        /**
         * Méthode en charge de créer ou récupérer l'instance unique de Repository
         * Permet d'en faire un Singleton
         * @return l'isntance du repository
         */
        fun getSonRepoInstance(application: Application) =
            instance?: synchronized(this) {
                instance?: SonRepositoryAudioAssetsImpl(application)
                        .also { instance = it }
            }
    }
}