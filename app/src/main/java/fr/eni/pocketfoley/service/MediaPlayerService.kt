package fr.eni.pocketfoley.service

import android.content.Intent
import android.content.res.AssetFileDescriptor
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.utils.AUDIO_ASSETS_DIRECTORY
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.FileNotFoundException

/** Classe service en charge de gérer le jeu des sons quand on clique sur les boutons */
class MediaPlayerService : LifecycleService(), MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    /** le binder qui va être donné au client */
    private val binder = MediaPlayerBinder()

    /** le bouton qui a été cliqué et dont on va jouer le son */
    private lateinit var boutonActuel: Bouton

    private val listeLecture = mutableListOf<Bouton>()
    private val _listeLectureData = MutableLiveData<List<Bouton>>()
    val listeLectureData: LiveData<List<Bouton>>
        get() = _listeLectureData

    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()

    /** le MediaPlayer qui va s'occuper de jouer le son */
    private var player: MediaPlayer? = null

    /** La classe interne rerépsentant l'objet Binder qui relie le Service au client (le fragment) */
    inner class MediaPlayerBinder : Binder() {
        /**
         * Retourne cette instance (this) du [MediaPlayerService]
         * pour que le client puisse en appeler ses méthodes publiques
         */
        fun getService(): MediaPlayerService = this@MediaPlayerService
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        Timber.i("onBind() Service")
        return binder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Timber.i("onUnbind() Service")
        if (player?.isPlaying!!) {
            player?.stop()
        }
        player?.release()
        if (this::runnable.isInitialized) {
            handler.removeCallbacks(runnable)
        }
        return false
    }

    override fun onCreate() {
        super.onCreate()
        Timber.i("onCreate() Service")
        player = MediaPlayer()
        initPlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("onDestroy() Service")
    }

    /** Méthode en charge d'initialiser la liste de lecture observable en LiveData */
    private fun initListe() {
        _listeLectureData.value = listeLecture
        listeLectureData.observe(this, Observer {
            if (it.isNotEmpty()) {
                playTheList()
            }
        })
    }

    init {
        Timber.i("bloc init{...} du service")
        initListe()
    }

    /** Méthode en charge d'initialiser le MediaPlayer avec ses Listeners et Attributs*/
    private fun initPlayer() {
        Timber.i("initPlayer()")
        // Mise en place des listeners
        player?.setOnCompletionListener(this)
        player?.setOnPreparedListener(this)
        player?.setOnErrorListener(this)

        // Définition des attributs audio
        val attributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .build()
        player?.setAudioAttributes(attributes)
    }

    /** méthode en charge d'ajouter le [Bouton] passé par le Fragment à la [listeLecture] */
    fun addBoutonToPlaylist(nouveauBouton: Bouton) {
        listeLecture.add(nouveauBouton)
        _listeLectureData.value = listeLecture
        Timber.i("${nouveauBouton.title} ajouté à la liste dont la longueur fait: ${listeLectureData.value?.size}")
    }

    /**
     * Méthode en charge de gérer la lecture de la liste (vérif si elle est vide,
     * si un lecture est en cours) et si c'est bon définir le bouton à jouer et lancer
     * la procédure de lecture du média
     */
    private fun playTheList() {
        Timber.i("playTheList()")
        if (listeLecture.isNotEmpty()) {
            if (!(player?.isPlaying!!)) {
                Timber.i("if (listeLecture.isNotEmpty())")
                boutonActuel = listeLecture.first()
                Timber.i("boutonActuel = ${boutonActuel.title}")
                playMedia()
            }
        } else {
            Timber.i("ListeLecture isEmpty")
        }
    }

    /** Méthode en charge de retirer le son qui vient d'être lu de la liste de lecture */
    private fun removeBoutonFromList() {
        Timber.i("remove bouton: ${listeLecture.find { bouton: Bouton -> bouton == boutonActuel }}")
//        listeLecture.removeFirstOrNull()
        listeLecture.remove(boutonActuel)
        _listeLectureData.value = listeLecture
    }

    /**
     * Méthode en charge de déclencher le lancement du jeu du son
     * Remet le player à zéro, récupère l'adresse du son à jouer, et lance la préparation asynchrone
     * La méthode callback onPrepared() se charge de la suite
     */
    private fun playMedia() {
        Timber.i("playMedia() bouton: $boutonActuel")
        player?.reset()

        if (boutonActuel.externalSound.isNotEmpty())
        {
            prepareSonExterne()
        }
        else
        {
            prepareSonInterne()
        }
        player?.prepareAsync()
    }

    /** Méthode en charge de préparer le player en lui fournissant le son interne comme dataSource */
    private fun prepareSonExterne()
    {
        val sonAddress = boutonActuel.externalSound
        Timber.i("sonAddressExterne =  $sonAddress")
        try
        {
            val descriptor = contentResolver.openFileDescriptor(Uri.parse(sonAddress), "r")
            val fileDescriptor = descriptor!!.fileDescriptor
            player?.setDataSource(fileDescriptor)
            descriptor.close()
        }
        catch (e: FileNotFoundException)
        {
            Timber.e("Erreur dans externalSound setDataSource($sonAddress). Message: ${e.message}")
            applicationContext.revokeUriPermission(Uri.parse(sonAddress), Intent.FLAG_GRANT_READ_URI_PERMISSION)
            prepareSonInterne()
        }
    }

    /** Méthode en charge de préparer le player en lui fournissant le son interne comme dataSource */
    private fun prepareSonInterne()
    {
        val sonAddress = AUDIO_ASSETS_DIRECTORY + boutonActuel.internalSound
        Timber.i("sonAddressInterne =  $sonAddress")
        try
        {
            val descriptor : AssetFileDescriptor = assets.openFd(sonAddress)
            player?.setDataSource(descriptor.fileDescriptor, descriptor.startOffset, descriptor.length)
            descriptor.close()
        }
        catch (e: Exception)
        {
            Timber.e("Erreur dans internalSound setDataSource($sonAddress). Message: ${e.message}")
        }
    }

    /**
     * Méthode Callback appelée quand le MediaPlayer est prêt
     * grâce au setOnPreparedListener
     * Défini les quelques paramètres restants puis lance le jeu du son
     */
    override fun onPrepared(mp: MediaPlayer)
    {
        Timber.i("onPrepared() MediaPlayer")
        val mpStart = boutonActuel.startTimeMillis
        mp.seekTo(mpStart.toInt())
        val mpStop = boutonActuel.stopTimeMillis
        val mpDuration = mp.duration
        val mpLoop = boutonActuel.loop
        val mpVolume = boutonActuel.volume/100f
        mp.setVolume(mpVolume, mpVolume)
        Timber.i("mpStart = $mpStart, mpStop = $mpStop, duration = $mpDuration, mpVolume = $mpVolume")
        mp.start()

        // Gestion du stopTime
        runnable = Runnable {
            if(listeLecture.isNotEmpty() && player?.currentPosition!! >= mpStop)
            {
                if (mpLoop)
                {
                    loopPlayer()
                }
                else
                {
                    stopPlayer()
                }
            }
            handler.postDelayed(runnable, 50)
        }
        handler.postDelayed(runnable, 50)
//        checkCurrentPos()
//        areWeDoneYet(duration)
    }

    /**
     * Méthode Callback appelée quand le MediaPlayer a fini de jouer
     * grâce au setOnCompletionListener
     */
    override fun onCompletion(mp: MediaPlayer?)
    {
        Timber.i("onCompletion() MediaPlayer")
        mp?.reset()
        if (boutonActuel.loop)
        {
            Timber.i("Loop = true")
            playTheList()
        }
        else
        {
            Timber.i("Loop = false")
            removeBoutonFromList()
        }
    }

    /**
     * Méthode Callback appelée quand le MediaPlayer rencontre une erreur
     * grâce au setOnErrorListener
     */
    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean
    {
        Timber.e("Erreur n°$what rencontrée dans le MediaPlayer")
        mp?.reset()
        return false
    }

    /** Méthode publique en charge de communiquer l'avancée du lecteur */
    fun getCurrentPosition() = player?.currentPosition

    /** Méthode publique en charge de communiquer l'état de lecture du lecteur */
    fun isPlayerPlaying() = player?.isPlaying

    /** Méthode publique en charge de stoper le lecteur */
    fun stopPlayer()
    {
        Timber.i("stopPlayer()")
        player?.reset()
        removeBoutonFromList()
    }

    private fun loopPlayer()
    {
        Timber.i("loopPlayer()")
        player?.reset()
        playTheList()
    }
}
