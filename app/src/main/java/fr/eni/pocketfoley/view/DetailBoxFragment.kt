package fr.eni.pocketfoley.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import fr.eni.pocketfoley.R
import fr.eni.pocketfoley.databinding.FragmentDetailBoxBinding
import fr.eni.pocketfoley.model.Box
import fr.eni.pocketfoley.utils.BOX_ID_1
import fr.eni.pocketfoley.utils.hideKeyboard
import fr.eni.pocketfoley.utils.verifTitleLength
import fr.eni.pocketfoley.viewmodel.DetailBoxViewModel
import fr.eni.pocketfoley.viewmodel.DetailBoxViewModelFactory
import timber.log.Timber

/**
 * Un [Fragment] représentant la vue associée au détail d'une Box.
 */
class DetailBoxFragment : Fragment()
{
    private lateinit var binding: FragmentDetailBoxBinding

    private lateinit var detailBoxViewModel: DetailBoxViewModel

    private var boxIdArg : Long = BOX_ID_1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        (activity as AppCompatActivity).supportActionBar?.show()

        // récupération de l'identifiant de la Box à afficher transmis par SafeArg
        boxIdArg = DetailBoxFragmentArgs.fromBundle(requireArguments()).boxId

        // Inflate and bind the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentDetailBoxBinding>(inflater, R.layout.fragment_detail_box, container, false)

        // Récupération du contexte d'application
        val application = requireNotNull(this.activity).application

        // Création de l'instance de la ViewModel Factory.
        val viewModelFactory = DetailBoxViewModelFactory(boxId = boxIdArg, application = application)

        // Récup de la référence au viewModel associé à ce fragment
        detailBoxViewModel =  ViewModelProvider(this, viewModelFactory).get(DetailBoxViewModel::class.java)

        // Mise en place du dataBinding
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            boxViewModel = detailBoxViewModel
            etDetailBoxEditTitle.setOnEditorActionListener { v, _, event ->
                if (event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    if(onValidateBoxEdit())
                    {
                        v.findNavController().navigate(DetailBoxFragmentDirections.actionBoxFragmentToBoutonsFragment(boxId = boxIdArg))
                    }
                }
                false
            }
        }
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onResume()
    {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.titre_fragment_detail_box)
    }

    /** Méthode en charge de mettre à jour une [Box] après avoir cliqué sur "enregistrer" */
    private fun onValidateBoxEdit() : Boolean
    {
        val titleEditText = binding.etDetailBoxEditTitle

        // Récup de la saisie de l'utilisateur dans l'EditText
        val editedTitle = titleEditText.text.toString()
        val titleOk = editedTitle.verifTitleLength()
        // Vérification de la longueur du titre
        if(titleOk)
        {
            // masquage du clavier après saisie
            titleEditText.hideKeyboard()

            // Récupération de la Box actuelle
            val box = detailBoxViewModel.box.value!!

            // Mise à jour du titre de la box
            box.title = editedTitle

            // update
            detailBoxViewModel.editBox(box)
            Timber.i("Nouvelle Box à éditer: $box")
        }
        else
        {
            Toast.makeText(context, R.string.erreur_titre, Toast.LENGTH_SHORT).show()
        }
        return titleOk
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_config_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return if(item.itemId == R.id.item_detail_config_menu_save)
        {
            if(onValidateBoxEdit())
            {
                requireView().findNavController().navigate(DetailBoxFragmentDirections.actionBoxFragmentToBoutonsFragment(boxId = boxIdArg))
            }
            true
        }
        else
        {
            requireView().hideKeyboard()
            super.onOptionsItemSelected(item)
        }
    }
}
