package fr.eni.pocketfoley.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController

import fr.eni.pocketfoley.R
import fr.eni.pocketfoley.databinding.FragmentConfigBoutonBinding
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.utils.*
import fr.eni.pocketfoley.viewmodel.DetailBoutonViewModel
import fr.eni.pocketfoley.viewmodel.DetailBoutonViewModelFactory
import timber.log.Timber
import java.io.File
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class ConfigBoutonFragment : Fragment() {

    private lateinit var binding: FragmentConfigBoutonBinding

    private lateinit var detailBoutonViewModel: DetailBoutonViewModel

    private var boutonIdArg = 0L

    private var displayOption = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()

        // récupération de l'identifiant de la Box à afficher transmis par SafeArg
        boutonIdArg = ConfigBoutonFragmentArgs.fromBundle(requireArguments()).boutonId

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_config_bouton, container, false)

        // Récup du contexte d'application
        val application = requireNotNull(this.activity).application

        // Création de l'instance de la ViewModel Factory.
        val viewModelFactory = DetailBoutonViewModelFactory(boutonId = boutonIdArg, application = application)

        // Récup de la référence au viewModel associé à ce fragment
        detailBoutonViewModel = ViewModelProvider(this, viewModelFactory).get(DetailBoutonViewModel::class.java)

        // mise en place des liens de DataBinding
        bind()
        initializeSpinner()
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onResume()
    {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.titre_fragment_config_bouton)
    }

    /**
     * Méthode en charge de réaliser les actions du DataBinding entre le Layout,
     * le Fragment et le ViewModel une fois celui-ci mis en place
     */
    private fun bind()
    {
        // Mise en place du dataBinding
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            configBoutonViewModel = detailBoutonViewModel

            spConfigBoutonEditStyle

            // gère l'envoi du formulaire quand l'utilisateur appuie sur "Entrée"
            etConfigBoutonEditTitle.setOnEditorActionListener { v, _, event ->
                if (event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if (onUpdateBouton()) {
                        // navigation vers BoxWithBoutons
                        v.findNavController().navigate(
                            ConfigBoutonFragmentDirections.actionConfigBoutonFragmentToBoutonsFragment(
                                detailBoutonViewModel.bouton.value!!.box
                            )
                        )
                    }
                }
                false
            }

            binding.btnConfigBoutonSelectImage.setOnClickListener { recupImage() }

            // Affichage de la valeur du volume en fonction de la modification de la seekbar
            skbConfigBoutonVolumeBarre.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener{
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    binding.tvConfigBoutonVolumeValeur.text = progress.formatToPercentString()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            })
            // Affichage de la valeur du début de l'extrait en fonction de la modification de la seekbar
            skbConfigBoutonDebutBarre.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    binding.tvConfigBoutonDebutValeur.text =
                        progress.convertSecondsToFormattedString()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    // mise à jour de la seekbar de fin d'extrait pour empêcher d'avoir le début après la fin
                    if (binding.skbConfigBoutonFinBarre.progress <= seekBar!!.progress) {
                        binding.skbConfigBoutonFinBarre.progress = seekBar.progress.inc()
                    }
                }
            })

            // Affichage de la valeur du début de l'extrait en fonction de la modification de la seekbar
            skbConfigBoutonFinBarre.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    binding.tvConfigBoutonFinValeur.text =
                        progress.convertSecondsToFormattedString()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {}
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    // mise à jour de la seekbar de début d'extrait pour empêcher d'avoir le début après la fin
                    if (binding.skbConfigBoutonDebutBarre.progress >= seekBar!!.progress) {
                        binding.skbConfigBoutonDebutBarre.progress = seekBar.progress.dec()
                    }
                }
            })
            // Naviguer vers la liste de sélection d'un son
            btnConfigBoutonSelectSon.setOnClickListener {
                it.findNavController().navigate(
                    ConfigBoutonFragmentDirections.actionConfigBoutonFragmentToListeSonsFragment(
                        detailBoutonViewModel.bouton.value!!.id
                    )
                )
            }
        }
    }

    /** Méthode en charge de mettre à jour un [Bouton] après avoir cliqué sur "enregistrer" */
    private fun onUpdateBouton(): Boolean
    {
        val titleEditText = binding.etConfigBoutonEditTitle

        // Récup de la saisie de l'utilisateur dans l'EditText
        val editedTitle = binding.etConfigBoutonEditTitle.text.toString()
        val titleOk = editedTitle.verifTitleLength()
        // Vérification de la longueur du titre
        if (titleOk) {
            // masquage du clavier après saisie
            titleEditText.hideKeyboard()

            // Si on est arrivé jusqu'ici c'est qu'on a cliqué sur un bouton existant
            // et qu'on a passé son ID en param au fragment. Le bouton ne peut donc
            // pas être null, d'où les !!
            val boutonMaj = detailBoutonViewModel.bouton.value!!

            boutonMaj.apply {
                title = editedTitle
                loop = binding.swConfigBoutonToggleBoucle.isChecked
                volume = binding.skbConfigBoutonVolumeBarre.progress
                startTimeMillis =
                    binding.skbConfigBoutonDebutBarre.progress.convertSecondstoMillis()
                stopTimeMillis = binding.skbConfigBoutonFinBarre.progress.convertSecondstoMillis()
            }
            Timber.i("Bouton mis à jour: $boutonMaj")
            detailBoutonViewModel.editBouton(boutonMaj)
        } else {
            Toast.makeText(context, R.string.erreur_titre, Toast.LENGTH_SHORT).show()
        }
        return titleOk
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.detail_config_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return if (item.itemId == R.id.item_detail_config_menu_save) {
            if (onUpdateBouton()) {
                if (displayOption > 0) {
                    updateSharedPreferences()
                }
                // navigation vers BoxWithBoutons
                requireView().findNavController().navigate(
                    ConfigBoutonFragmentDirections.actionConfigBoutonFragmentToBoutonsFragment(
                        detailBoutonViewModel.bouton.value!!.box
                    )
                )
            }
            true
        } else {
            requireView().hideKeyboard()
            super.onOptionsItemSelected(item)
        }
    }

    /**
     * Méthode en charge de récupérer une image en envoyant un Intent avec Action Open Document.
     * Permet d'ouvrir le navigateur de fichiers grâce au Storage Access Framework.
     * On filtre seulement les fichiers de type MIME image.
     */
    private fun recupImage()
    {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        intent.type = "image/*"
        startActivityForResult(intent, OPEN_IMAGE_FILE_REQUEST_CODE)
    }

    /**
     * Méthode qui s'exécute une fois la sélection de [recupImage] terminée.
     * Récupère l'URI du fichier auprès du ContentResolver en lui précisant qu'on veut persister
     * les autorisations d'accès à ladite URI, puis exécute la méthode [onSelectedImage]
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        var currentUri: Uri?
        var address = "vide"
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == OPEN_IMAGE_FILE_REQUEST_CODE) {
                data?.let {
                    currentUri = it.data
                    try
                    {
                        Timber.i("permissions: ${requireContext().contentResolver.persistedUriPermissions.size}")
                        requireContext().contentResolver.takePersistableUriPermission(
                            currentUri!!, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    }
                    catch (e : Exception)
                    {
                        Timber.e("Erreur dans la récup de l'image: ${e.message}")
                    }
                    try
                    {
                        address = currentUri.toString()
                        val file = File(address)
                        Timber.i("file.path =  ${file.path}")
                        onSelectedImage(address)

                    }
                    catch (e: IOException)
                    {
                        Timber.e("Erreur dans la récup de l'image: ${e.message}")
                    }
                }
            }
        }
        Timber.i("adresse récupérée: $address")
    }

    /**
     * Méthode en charge de mettre à jour le [Bouton] avec une nouvelle image
     * @param imageUri l'URI du fichier associé convertie en String
     */
    private fun onSelectedImage(imageUri: String)
    {
        val boutonMaj = detailBoutonViewModel.bouton.value
        Timber.i("detailBoutonViewModel.bouton.value = $boutonMaj")
        boutonMaj!!.image = imageUri
        Timber.i("boutonMaj = $boutonMaj")
        detailBoutonViewModel.editBouton(boutonMaj)
    }

    /** Méthode en charge d'initialiser le Spinner avec les différentes options d'affichages */
    private fun initializeSpinner()
    {
        val spinnerOptions = arrayOf(SPINNER_OPTION_TITLE, SPINNER_OPTION_IMAGE, SPINNER_OPTION_BOTH)
        val spinnerStrOptions = arrayOf(
            requireContext().getString(R.string.style_bouton_choix_titre),
            requireContext().getString(R.string.style_bouton_choix_image),
            requireContext().getString(R.string.style_bouton_choix_titre_et_image)
        )

        val arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, spinnerStrOptions)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val sp: SharedPreferences = requireContext().getSharedPreferences(
            BOUTON_DISPLAY_PREFERENCES, Context.MODE_PRIVATE)
        val optionFromPref = sp.getInt(boutonIdArg.toString(), 0)

        binding.apply {
            spConfigBoutonEditStyle.adapter = arrayAdapter
            if (optionFromPref > 0)
            {
                spConfigBoutonEditStyle.setSelection(spinnerOptions.indexOf(optionFromPref))
                Timber.i("Sélection récupérée des SharedPreferences: $optionFromPref")
            }
            spConfigBoutonEditStyle.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
                    {
                        displayOption = spinnerOptions[position]
                        Timber.i("Item selected: $displayOption - ${spinnerStrOptions[position]}")
                    }
                }
        }
    }

    /** Méthode en charge de mettre à jour les SharedPreferences avec l'option d'affichage choisie */
    private fun updateSharedPreferences()
    {
        val sp: SharedPreferences = requireContext().getSharedPreferences(
            BOUTON_DISPLAY_PREFERENCES, Context.MODE_PRIVATE
        )
        sp.edit().putInt(boutonIdArg.toString(), displayOption).apply()
    }
}
