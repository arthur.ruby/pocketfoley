package fr.eni.pocketfoley.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import fr.eni.pocketfoley.R
import fr.eni.pocketfoley.databinding.ActivityMainBinding
import timber.log.Timber

class MainActivity : AppCompatActivity()
{

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		Timber.i("Méthode onCreate")

		@Suppress("UNUSED_VARIABLE")
		val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

		// find the controller from the ID of our NavHostFragment using the KTX extension function.
		val navController = this.findNavController(R.id.appNavHostFragment)

		// Link the NavController to our ActionBar.
		NavigationUI.setupActionBarWithNavController(this, navController)
	}

	// Override the onSupportNavigateUp method from the activity and call navigateUp in nav controller.
	override fun onSupportNavigateUp(): Boolean
	{
		val navController = this.findNavController(R.id.appNavHostFragment)
		return navController.navigateUp()
	}
}
