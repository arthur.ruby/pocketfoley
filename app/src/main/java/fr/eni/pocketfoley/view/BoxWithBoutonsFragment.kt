package fr.eni.pocketfoley.view

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.Bundle
import android.os.IBinder
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

import fr.eni.pocketfoley.R
import fr.eni.pocketfoley.adapter.BoutonAdapter
import fr.eni.pocketfoley.adapter.BoutonListener
import fr.eni.pocketfoley.adapter.BoutonLongListener
import fr.eni.pocketfoley.databinding.FragmentBoxWithBoutonsBinding
import fr.eni.pocketfoley.service.MediaPlayerService
import fr.eni.pocketfoley.utils.*
import fr.eni.pocketfoley.viewmodel.BoxWithBoutonsViewModel
import fr.eni.pocketfoley.viewmodel.BoxWithBoutonsViewModelFactory
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class BoxWithBoutonsFragment : Fragment()
{
    /** Le service en charge de jouer les sons */
    private lateinit var mediaService : MediaPlayerService

    /** Le "flag" qui permet de suivre l'état de connexion au service */
    private var serviceBound = false

    /** le viewModel associé à cette vue */
    private lateinit var boxWithBtnsViewModel: BoxWithBoutonsViewModel

    /** l'objet binding faisant le lien entre ce controller et sa vue */
    private lateinit var binding: FragmentBoxWithBoutonsBinding

    /** l'identifiant de la box à afficher récupéré plus tard en SafeArg de navigation */
    private var boxIdArg = BOX_ID_1

    /** utilisé pour suivre la mise à jour de l'IHM des boutons en jeu */
    private var previousBoutonPosInDataSet = 0

    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection = object : ServiceConnection
    {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?)
        {
            Timber.i("onServiceConnected to Fragment")
            val binder = service as MediaPlayerService.MediaPlayerBinder
            mediaService = binder.getService()
            serviceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName?)
        {
            Timber.i("onServiceDisconnected from Fragment")
            serviceBound = false
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        Timber.i("onCreatedView Fragment")

        // Masque la ToolBar sur l'écran principal
        (activity as AppCompatActivity).supportActionBar?.hide()

        // Inflate and bind the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_box_with_boutons, container, false)

        instantiateViewModel()

        mediaService = MediaPlayerService()

        // Mise en place de tous les DataBindings restants sur le Fragment
        bindBoutonsAdapter()
        bindNavBar()

        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            boxWithBoutonsViewModel = boxWithBtnsViewModel
        }
        return binding.root
    }

    override fun onStart()
    {
        Timber.i("onStart Fragment")
        super.onStart()
        initializeService()
    }

    override fun onResume()
    {
        Timber.i("onResume Fragment")
        super.onResume()
    }

    override fun onStop()
    {
        Timber.i("onStop Fragment")
        super.onStop()
        activity?.unbindService(connection)
        serviceBound = false
    }

    private fun instantiateViewModel()
    {
        // récupération de l'identifiant de la Box passé en param via safeArgs
        boxIdArg = BoxWithBoutonsFragmentArgs.fromBundle(requireArguments()).boxId

        // Récupération du context d'application pour la ViewModelFactory
        val application = requireNotNull(this.activity).application

        // Récupération de la factory de ViewModel
        val viewModelFactory = BoxWithBoutonsViewModelFactory(boxId = boxIdArg, application = application)

        // Création du viewModel grâce à sa factory
        boxWithBtnsViewModel = ViewModelProvider(this, viewModelFactory).get(BoxWithBoutonsViewModel::class.java)
    }

    /** méthode en charge de réaliser les bindings entre la navBar et les données du ViewModel */
    private fun bindNavBar()
    {
        val navBar = binding.nvbrBoxesBottomNavBar
        // On précisé "manuellement" à la navBar quel menuItem est sélectionné
        // en fonction de l'identifiant de la box sélectionnée
        val itemId = when(boxIdArg) {
            BOX_ID_1 -> R.id.btnav_bottom_nav_box1
            BOX_ID_2 -> R.id.btnav_bottom_nav_box2
            else -> R.id.btnav_bottom_nav_box3
        }
        navBar.selectedItemId = itemId

        val dataBoxes = boxWithBtnsViewModel.boxes
        dataBoxes.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it.isNotEmpty())
                    navBar.setOnNavigationItemSelectedListener { menuItem ->
                        // Si on clique sur la box actuelle
                        if (navBar.selectedItemId == menuItem.itemId)
                        {
                            // On va vers sa page d'édition
                            requireView().findNavController().navigate(BoxWithBoutonsFragmentDirections.actionBoutonsFragmentToBoxFragment(boxIdArg))
                        }
                        //Sinon, on lance la navigation vers la box sélectionnée
                        else
                        {
                            // On détermine quel menuItem a été sélectionné
                            // et on attribue la valeur de la box vers laquelle naviguer en conséquence
                            val navId = when (menuItem.itemId)
                                {
                                    R.id.btnav_bottom_nav_box1 ->  BOX_ID_1
                                    R.id.btnav_bottom_nav_box2 ->  BOX_ID_2
                                    else ->  BOX_ID_3
                                }
                            menuItem.isChecked = true
                            requireView().findNavController().navigate(
                                BoxWithBoutonsFragmentDirections.actionBoutonsFragmentSelf(navId)
                            )
                        }
                        true
                    }

                // Mise en place des noms des boxes
                for (wololo in 0..it.lastIndex)
                {
                    navBar.menu[wololo].apply { title = it[wololo].title }
                }
            }
        })

    }

    /** Méthode en charge de lier l'adapter du recyclerView des boutons au Fragment */
    private fun bindBoutonsAdapter()
    {
        // Création d'un adapter avec le constructeur de la classe qu'on a créé
        // et ajout d'un clickListener personnalisé sur ses éléments
        val adapter = BoutonAdapter(
            BoutonListener { boxWithBtnsViewModel.onBoutonClicked(it) },
            BoutonLongListener { boxWithBtnsViewModel.onBoutonLongClicked(it) },
            context = requireContext()
        )

        // Mise en place de l'observer sur la propriété de navigation du ViewHolder
        // déclenche la navigation vers le détail du bouton dont l'identifiant est contenu
        // dans la propriété du ViewHolder
        boxWithBtnsViewModel.navigateToConfigBouton.observe(viewLifecycleOwner, Observer {
            it?.let {
                this.findNavController().navigate(BoxWithBoutonsFragmentDirections.actionBoutonsFragmentToConfigBoutonFragment(it))
                boxWithBtnsViewModel.onConfigBoutonNavigated()
            }
        })

        // Mise en place de l'observer sur la liste des boutons du viewModel qui permet de mettre
        // à jour automatiquement la liste des boutons de l'adapter
        val dataBoxWithBoutons = boxWithBtnsViewModel.boxWithBoutons

        dataBoxWithBoutons.observe(viewLifecycleOwner, Observer {
            it?.let {
                // permet de mettre à jour automatiquement la liste de l'Adapter
                // avec celle du LiveData du ViewModel
                adapter.submitList(it.boutons)
            }
        })
        binding.rvBoutonsRecyclerView.adapter = adapter

    }

    /** méthode en charge d'initialiser le service de MediaPlayer */
    private fun initializeService()
    {
        // Initialisation du service et reliage au fragment par le Binder
        Intent(activity, MediaPlayerService::class.java).also {
            activity?.bindService(it, connection, Context.BIND_AUTO_CREATE)
        }

        // Mise en place de l'observer sur le LiveData du viewModel contenant le bouton qui a été cliqué
        // On fait ça dans le onStart et après l'initialisation du Service sinon ça marche pas
        boxWithBtnsViewModel.boutonClicked.observe(viewLifecycleOwner, Observer {
            it?.let {
                // On vérifie le flag: si serviceBound, alors mediaService a bien été initialisé.
                // et on peut accéder à ses méthodes.
                if(serviceBound)
                {
                    Timber.i("Bouton in Fragment = $it")
                    if(mediaService.isPlayerPlaying()!! && it == mediaService.listeLectureData.value?.first())
                    {
                        mediaService.stopPlayer()
                    }
                    else
                    {
                        mediaService.addBoutonToPlaylist(it)
                        majAffichageBoutonLecture()
                    }
                }
            }
        })
    }

    // TODO Trouver un moyen de faire marcher ça, sachant qu'avec le changement de box on supprime
    //  le Fragment et le ViewModel de la précédente, du coup sa liste de bouton n'est plus
    //  accessible pour récupérer le ViewHolder approprié, et ça disparaît.
    private fun majAffichageBoutonLecture()
    {
        Timber.i("majAffichageBoutonLecture()")
        if(serviceBound)
        {
            Timber.i("service is bound")
            mediaService.listeLectureData.observe(viewLifecycleOwner, Observer { playlistBoutons ->
                val previousBoutonViewHolder : BoutonAdapter.BoutonViewHolder =
                    binding.rvBoutonsRecyclerView.findViewHolderForAdapterPosition(previousBoutonPosInDataSet)
                            as BoutonAdapter.BoutonViewHolder
                previousBoutonViewHolder.binding.ivBoutonRecyclerItemPlayingIcon.let { it.visibility = View.INVISIBLE }
                if (playlistBoutons.isNotEmpty())
                {
                    val firstItemId = playlistBoutons.first().id.toInt()
                    Timber.i("playlist first item id = $firstItemId")
                    val currentBoutonPosInDataSet = boxWithBtnsViewModel.boxWithBoutons.value?.boutons?.indexOf(playlistBoutons.first()) ?: -1
                    Timber.i("currentBoutonPosInDataSet = $currentBoutonPosInDataSet")
                    if (currentBoutonPosInDataSet > -1)
                    {
                        Timber.i("Dans CurrentViewHolder avec position = $currentBoutonPosInDataSet")
                        val vh = binding.rvBoutonsRecyclerView.findViewHolderForAdapterPosition(currentBoutonPosInDataSet)
                        val currentBoutonViewHolder : BoutonAdapter.BoutonViewHolder = vh as BoutonAdapter.BoutonViewHolder
                        currentBoutonViewHolder.binding.ivBoutonRecyclerItemPlayingIcon.let { it.visibility = View.VISIBLE}
                        previousBoutonPosInDataSet = currentBoutonPosInDataSet
                    }
                }
            })
        }
        else
        {
            Timber.i("service not bound")
        }
    }
}
