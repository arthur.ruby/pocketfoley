package fr.eni.pocketfoley.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import fr.eni.pocketfoley.R
import fr.eni.pocketfoley.adapter.SonAdapter
import fr.eni.pocketfoley.adapter.SonListener
import fr.eni.pocketfoley.databinding.FragmentListeSonsBinding
import fr.eni.pocketfoley.model.Bouton
import fr.eni.pocketfoley.utils.AUDIO_ASSETS_DIRECTORY
import fr.eni.pocketfoley.utils.OPEN_AUDIO_FILE_REQUEST_CODE
import fr.eni.pocketfoley.utils.getInternalAudioFileDuration
import fr.eni.pocketfoley.utils.getExternalAudioFileDuration
import fr.eni.pocketfoley.viewmodel.SonInterneViewModel
import fr.eni.pocketfoley.viewmodel.SonInterneViewModelFactory
import timber.log.Timber
import java.io.File
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class ListeSonsFragment : Fragment()
{
    private lateinit var sonInterneViewModel : SonInterneViewModel

    private var boutonIdArg : Long = 0L

    private lateinit var binding: FragmentListeSonsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View?
    {
        Timber.i("onCreateView")

        // récupération de l'identifiant du [Bouton] à qui on est lié transmis par SafeArg
        boutonIdArg = ListeSonsFragmentArgs.fromBundle(requireArguments()).boutonId

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_liste_sons, container, false)

        // Récupération du context d'application pour la ViewModelFactory
        val application = requireNotNull(this.activity).application

        // Récupération de la factory de ViewModel
        val factory = SonInterneViewModelFactory(application)

        // Création du viewModel grâce à sa factory
        sonInterneViewModel = ViewModelProvider(this, factory).get(SonInterneViewModel::class.java)

        bindSonAdapter()

        return binding.root
    }

    override fun onResume()
    {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.titre_fragment_selectionner_son)
    }

    /** Méthode en charge de lier l'adapter du recyclerView des sons au Fragment */
    private fun bindSonAdapter()
    {
        // Création d'un adapter avec le constructeur de la classe qu'on a créé
        val adapter = SonAdapter(SonListener {
            Toast.makeText(context, "Click sur son interne n° $it", Toast.LENGTH_SHORT).show()
            onSelectedInternalSound(it)
            sonInterneViewModel.onSonClicked(boutonIdArg)
        })

        // Mise en place de l'observer sur la propriété de navigation du ViewHolder
        // déclenche la navigation vers le détail du bouton dont l'identifiant est contenu
        // dans la propriété du ViewHolder
        sonInterneViewModel.navigateToConfigBouton.observe(viewLifecycleOwner, Observer {
            it?.let {
                this.findNavController().navigate(ListeSonsFragmentDirections.actionListeSonsFragmentToConfigBoutonFragment(boutonIdArg))
                sonInterneViewModel.onConfigBoutonNavigated()
            }
        })

        // Récupération de la liste des fichiers sons internes
        val listeSonsAssets = sonInterneViewModel.listeAudioAssets

        // Mise en place d'un observer sur la liste de sons du ViewModel pour lui dire de mettre à jour
        // automatiquement la liste de l'Adapter avec celle-ci
        listeSonsAssets.observe(viewLifecycleOwner, Observer {
            it?.let {
                Timber.i("observation de la listeSonAssets de taille: ${it.size}")
                adapter.submitList(it) } })

        // On met en place le clicListener sur le bouton chargé de récupérer un son perso
        binding.btnFabAjoutSonPerso.setOnClickListener { recupSonPerso() }

        // On relie l'adapter à la propriété adapter du RecyclerView
        binding.rvSonsInternesRecyclerView.adapter = adapter
    }

    /**
     * Méthode en charge de mettre à jour le [Bouton] avec le nouveau son interne
     * @param sonAdress l'adresse du fichier son interne dans le répertoire Assets
     */
    private fun onSelectedInternalSound(sonAdress : String)
    {
        val boutonMaj = sonInterneViewModel.bouton.value
        Timber.i("sonInterneViewModel.bouton.value = $boutonMaj")
        boutonMaj!!.internalSound = sonAdress
        boutonMaj.externalSound = ""
        boutonMaj.startTimeMillis = 0L
        boutonMaj.stopTimeMillis = (AUDIO_ASSETS_DIRECTORY + sonAdress).getInternalAudioFileDuration(requireContext())
        sonInterneViewModel.editBouton(boutonMaj)
    }

    /**
     * Méthode en charge de mettre à jour le [Bouton] avec le nouveau son externe
     * @param sonUri l'URI du fichier associé convertie en String
     */
    private fun onSelectedExternalSound(sonUri : String)
    {
        val boutonMaj = sonInterneViewModel.bouton.value
        Timber.i("sonInterneViewModel.bouton.value = $boutonMaj")
        boutonMaj!!.externalSound = sonUri
        boutonMaj.startTimeMillis = 0L
        boutonMaj.stopTimeMillis = sonUri.getExternalAudioFileDuration(requireContext())
        Timber.i("boutonMaj = $boutonMaj")
        sonInterneViewModel.editBouton(boutonMaj)
        sonInterneViewModel.onSonClicked(boutonIdArg)
    }

    /**
     * Méthode en charge de récupérer un son Externe en envoyant un Intent avec Action Open Document.
     * Permet d'ouvrir le navigateur de fichiers grâce au Storage Access Framework.
     * On filtre seulement les fichiers de type MIME audio/mpeg pour que seuls les mp3 soient sélectionnables.
     */
    private fun recupSonPerso()
    {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
        intent.type = "audio/mpeg"
        startActivityForResult(intent, OPEN_AUDIO_FILE_REQUEST_CODE)
    }

    /**
     * Méthode qui s'exécute une fois la sélection de [recupSonPerso] terminée.
     * Récupère l'URI du fichier auprès du ContentResolver en lui précisant qu'on veut persister
     * les autorisations d'accès à ladite URI, puis exécute la méthode [onSelectedExternalSound]
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        var currentUri: Uri?
        var address = "vide"
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == OPEN_AUDIO_FILE_REQUEST_CODE)
            {
                data?.let {
                    currentUri = it.data
                    Timber.i("permissions: ${requireContext().contentResolver.persistedUriPermissions.size}")
                    requireContext().contentResolver.takePersistableUriPermission(currentUri!!, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    try {

                        address = currentUri.toString()
                        val file = File(address)
                        Timber.i("file.path =  ${file.path}")
                        onSelectedExternalSound(address)

                    } catch (e: IOException)
                    {
                        Timber.e("Erreur dans la récup du son externe: ${e.message}")
                    }
                }
            }
        }
        Timber.i("adresse récupérée: $address")
    }

}
