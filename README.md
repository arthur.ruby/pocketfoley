![splash screen](/screenshots/splash_screen_phone.png)

# Présentation

PocketFoley est une application de boîte à sons.

Compatibilité testée: Android 8.0 à Android 10

L’application possède trois boxes dont le titre est modifiable. Une box contient 12 boutons et chaque bouton est associé à un son configurable (choisir le début, la fin, le volume, s’il tourne en boucle).

Lorsque l’on clique sur un bouton, il émet le son associé. Si on appuie sur trois boutons d’affilé, les sons sortent les uns à la suite des autres. Il est possible de personnaliser le son, ajouter une image au bouton ou modifier son titre.

Une fois l'application installée, aucune connexion internet n'est requise.

Aucune donnée personnelle n'est collectée.

# Fonctionnalités
## Navigation :
- Naviguer entre les boxes
- Naviguer vers les options de configuration d’un bouton
- Naviguer dans un explorateur des fichiers de l’appareil

## Gestion des boxes :

- Modifier le nom d’une box

## Gestion des boutons :

- Jouer un son
- Mettre un son en file d’attente
- Définir la lecture simple ou en boucle de l’extrait sonore associé au bouton
- Définir le volume de l’extrait sonore associé au bouton
- Définir le début de l’extrait sonore associé au bouton
- Définir la fin de l’extrait sonore associé au bouton
- Modifier le nom d’un bouton
- Ajouter une image au bouton
- Modifier le style du bouton entre affichage du titre, du titre et de l’image, ou de l’image

## Gestion des sons :

- Attribuer un son interne de l’application à un bouton
- Attribuer un son externe provenant de l’appareil à un bouton

## Screenshots :
![écran d'accueil](/screenshots/box_with_boutons.png)
![configuration box](/screenshots/config_box.png)
![configuration bouton](/screenshots/config_bouton.png)
![sélection son interne](/screenshots/selection_son_interne.png)
